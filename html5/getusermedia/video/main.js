/*
  exemple simple d'usage de la balise vidéo
  basé sur : https://davidwalsh.name/browser-camera
*/

/* objet dont on va se servir pour regrouper nos paramètres
   sans trop encombrer l'environnement global */
var parametres = {
  largeur: 320,
  hauteur: 230,
}

var body = document.getElementById('body');

function lancer () {
  /* cette fonction va tester le support de la vidéo par le navigateur
     et le cas échéant :
     * supprimer le message d'erreur
     * mettre en place les éléments html nécessaires
     * lancer la requête getusermedia video
     * ajouter les écouteurs nécessaires pour que
       lorsque l'on clique sur le bouton 'prendre une photo'
       une capture fixe de la vidéo soit faite et placée dans le canvas
  */

  // Le test en question
  if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {

    // si le support est détecté, on imprime un message dans la console
    console.info('support vidéo détecté');

    // on supprime le message d'erreur
    supprimer_le_message_d_erreur();

    // on ajoute les éléments html nécéssaires
    ajouter_elements_video();

    // on lance la requête getusermedia et on déclare l'écouteur
    // pour le bouton
    lancement_getusermedia_et_ecouteur_bouton();
  // si le test échoue
  } else {

    // on imrpime un message dans la console
    console.error('le support vidéo n\'a pas été détecté');
  }
}

function supprimer_le_message_d_erreur() {
  /* supprime le message d'erreur */
  var message_d_erreur = document.getElementById('message_d_erreur');
  message_d_erreur.parentElement.removeChild(message_d_erreur);
}


function ajouter_elements_video() {
  /* cette fonction ajoute au DOM les éléments HTML nécéssaires :
     * <video>
     * <button>
     * <canvas>
  */

  // l'élément video et ses attributs
  var video = document.createElement('video');
  video.setAttribute('id', 'video');
  video.setAttribute('width', parametres.largeur);
  video.setAttribute('height', parametres.hauteur);
  video.setAttribute('autoplay', true);
  body.appendChild(video);

  // l'élément button et ses attributs
  var bouton = document.createElement('button');
  var contenu_bouton = document.createTextNode("prendre une photo");
  bouton.appendChild(contenu_bouton);
  bouton.setAttribute('id', 'bouton');
  body.appendChild(bouton);

  // l'élément canvas
  var canvas_photo = document.createElement('canvas');
  canvas_photo.setAttribute('id', 'canvas');
  canvas_photo.setAttribute('width', parametres.largeur);
  canvas_photo.setAttribute('height', parametres.hauteur);
  body.appendChild(canvas_photo);
}

function lancement_getusermedia_et_ecouteur_bouton() {
  /* lance la requête getusermedia
     et déclare l'écouteur pour le bouton 'prendre une photo' */

  // On cible les éléments nécéssaires du DOM
  var canvas = document.getElementById('canvas');
  var video = document.getElementById('video');
  var bouton = document.getElementById("bouton");

  // on créé le context pour le dessin dans le canvas
  var context = canvas.getContext('2d');

  // requête getusermedia avec juste la vidéo
  navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
    console.log(stream)
    video.src = window.URL.createObjectURL(stream);
    video.play();
  });

  /* on déclare l'événement clic sur l'élément bouton
     qui va permettre de prendre la photo */
  bouton.addEventListener("click", function() {
    // on dessine l'image lorsque l'événement est détecté
  	context.drawImage(video, 0, 0, parametres.largeur, parametres.hauteur);
  });
}

// on lance le tout
lancer();
