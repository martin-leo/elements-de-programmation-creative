# Créer et ajouter dynamiquement des éléments dans un document HTML

Ce exemple montre comment ajouter en JavaScript des éléments dans un document HTML, après lui avoir ajouté un attribut classe ainsi qu'une valeur, ainsi que des styles via l'attribut style.

Utilise :

* [document.getElementById](https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById)
* [document.createElement](https://developer.mozilla.org/en-US/docs/Web/API/Document/createElement)
* [Element.setAttribute](https://developer.mozilla.org/en-US/docs/Web/API/Element/setAttribute)
* [Node.cloneNode](https://developer.mozilla.org/en-US/docs/Web/API/Node/cloneNode)
* [Element.innerHTML](https://developer.mozilla.org/en-US/docs/Web/API/Element/innerHTML)
* [HTMLElement.style](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/style)
* [Node.appendChild](https://developer.mozilla.org/en-US/docs/Web/API/Node/appendChild)
* [Node.insertBefore](https://developer.mozilla.org/en-US/docs/Web/API/Node/insertBefore)
