// on récupère une référence à notre élément conteneur grâce à son id
var conteneur = document.getElementById("conteneur");

// on créé un nouvel élément
var nouvel_element = document.createElement("div");

// on lui ajoute une classe
nouvel_element.setAttribute("class", "nouvel_element");

// on le duplique
clone = nouvel_element.cloneNode(true);

// on ajoute du contenu à l'original (ici avec innerHTML)
nouvel_element.innerHTML = "premier élément ajouté en JavaScript";

// on ajoute du contenu au clone
clone.innerHTML = "autre élément ajouté en JavaScript";

// on modifie la propriété CSS background-color de notre élément
// /!\ directement dans le DOM
nouvel_element.style.backgroundColor = "rgb(0,200,200)";

// on place notre élément dans le conteneur à la fin de celui-ci
conteneur.appendChild(nouvel_element);

// on place le clone juste avant notre conteneur,
// dans l'élément parent de celui-ci
conteneur.parentNode.insertBefore(clone, conteneur);
