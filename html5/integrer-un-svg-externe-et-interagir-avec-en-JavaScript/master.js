// on récupère une référence à notre élément object grâce à son attrbut id
var element_object_svg = document.getElementById('element-svg');

function re_colorier( element ) {
  /* change la couleur de remplissage d'un élément donné pour une couleur aléatoire. */

  // nombre aléatoire dans la limite du nombre de couleurs disponibles dan sla variable couleurs définie dans couleurs.js
  var n = Math.floor( Math.random() * couleurs.length );

  // on prend la couleur correspondant à n dans la liste des couleurs et on change la valeur de l'attribut fill de l'élément pour celle-ci
  element.setAttribute('fill', couleurs[n]);
}

/* on ne peut pas travailler sur notre svg avant que celui-ci ne soit chargé.
On doit donc attendre que l'événement onload associé à notre élément svg soit lancé */
element_object_svg.onload = function () {

  // petit message pour signaler que le SVG est chargé
  console.log('svg chargé');

  // on récupère une référence audit SVG
  var svg = this.contentDocument;

  // on récupère la liste des éléments avec la classe bouton
  var boutons = svg.getElementsByClassName('bouton');

  // pour tous les éléments de la liste
  for (var i = 0; i < boutons.length; i++) {
    // on ajoute un événement clic
    boutons[i].addEventListener( 'click', function (evenement) {
      // qui appelle cette fonction qui appele re_colorier avec en argument la référence à l'élément cliqué (la cible de l'événement)
      re_colorier( evenement.target );
    }, false);
  }
}

// petit message pour signaler que le js est chargé
console.log('js chargé');
