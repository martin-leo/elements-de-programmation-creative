# intégrer un SVG externe et interagir avec en JavaScript

Cet exemple montre comment intégrer à du HTML un SVG externe via l'élément HTML object, tout en pouvant y accéder en JavaScript, et par exemple lui ajouter des écouteurs.

## notes

* cet exemple utilise deux feuilles de style, une habituelle, l'autre pour le SVG. Cette dernière est appelée à la deuxième ligne du fichier SVG.

## sources

* [JavaScript et SVG : manipuler des noeuds SVG avec des boutons](http://tecfa.unige.ch/perso/mafritz/teaching/stic-1/svg/javascript-svg-ex1.html)
* [Fills and Strokes : using CSS](https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Fills_and_Strokes#Using_CSS)
* [SVG - getting started](https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Getting_Started)
