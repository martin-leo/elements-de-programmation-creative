# servir le contenu d'un répertoire

Le module http-server rend très simple le déploiement d'un serveur minimal permettant de servir le contenu d'un répertoire.

Pour cela :

1. ouvrir le terminal
2. si http-server n'est pas installé, taper ```npm install http-server -g```
6. appuyer sur entrée et patienter pendant l'installation
3. taper ```cd```, **suivi d'une espace**
4. glisser-déposer le répertoire que l'on souhaite servir dans le terminal
5. le chemin de celui-ci est ajouté après ```cd```, on obtient ```cd /chemin/du/repertoire```
6. appuyer sur entrée, ce qui suit apparait :

```bash
Starting up http-server, serving ./
Available on:
  http://127.0.0.1:8080
  http://192.168.100.100:8080
```

Il s'agit des url où le répertoire est accessible via navigateur, respectivement sur la machine elle-même et sur le réseau local.
