# Johnny Five : blink

Cet exemple reprend le sketch élémentaire permettant de faire clignoter la diode électro-luminescente intégrée d'une carte Arduino Uno ou de toute autre [carte arduino compatible](https://johnny-five.io/platform-support/) avec Johnny-Five et disposant d'une led intégrée.

C'est un bon moyen de tester une carte sur laquelle le micrologiciel Firmata aurait été installé.

# nota bene

Selon la carte, le *pin* de la diode électro-luminescente intégrée peut changer !

N'oubliez pas d'installer au préalable johnny-five sur votre machine à l'aide de la commande ```npm install``` (cette commande regarde le contenu du fichier ```package.json``` et installe automatiquement toutes les dépendances qui y sont renseignées).
