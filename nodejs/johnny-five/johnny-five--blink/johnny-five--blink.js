// on intègre la librairie johnny-five
var five = require("johnny-five");

// on créé un objet carte
var board = new five.Board();

// la diode électroluminescente intégrée est connectée au pin 13 sur une Arduino Uno
const pin_diode_electroluminescente = 13;

// lorsque l'objet carte déclenche l'événement "prêt" (ce qui signifie que l'on s'est connecté à la carte avec succès)
board.on( "ready", function () {

  // on va créer un objet Led (diode électroluminescente) et on lui donne comme argument la référence au pin correspondant
  var diode_electroluminescente = new five.Led ( pin_diode_electroluminescente );

  // on appelle la méthode blink (clignoter) de l'objet Led, en lui donnant comme argument 500 (ce sont des millisecondes) : on va alors faire clignoter la led toutes les 500 millisecondes
  diode_electroluminescente.blink ( 500 );
});
