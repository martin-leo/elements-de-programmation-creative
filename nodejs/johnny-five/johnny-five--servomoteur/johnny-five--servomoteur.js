// on intègre la librairie johnny-five
var five = require("johnny-five");

// on créé un objet carte
var board = new five.Board();

// la configuration de notre servo (tout est optionnel sauf la référence du pin)
var configuration_servomoteur = {
    // id: "mon-cervomoteur", // identifiant défini par l'utilisateur
    pin: 10,                  // pin PWM sur lequel est connecté le servomoteur
    // range: [0,180],        // l'amplitude du servo : par défaut: 0-180
    // type: "standard",      // par défaut : "standard". On utilisera "continuous" pour un servomoteur continu
    // startAt: 90,           // va à la position demandé au démarrage
    // offset: false,         // une valeur positive ou négative pour ajuster le servomoteur. défaut : false
    // invert: false,         // inverse le mouvement du servo. défaut : false.
    // center: true,          // centre le servomoteur au démarrage
    // fps: 100,              // variable utilisée pour le calcul du nombre de mouvements entre les positions
    // controller: DEFAULT,   // type d'interface : DEFAULT, PCA9685, etc.
  };

// lorsque l'objet carte déclenche l'événement "prêt" (on s'est connecté à la carte avec succès)
board.on( "ready", function () {

  var servo = new five.Servo( configuration_servomoteur );

  // mouvement aller-retour infini
   servo.sweep();

  // on positionne le servomoteur à sa position minimum
  //servo.min();


  // on positionne le servomoteur à sa position maximum
  //servo.max();


  // on positionne le servomoteur à sa position centrale
  //servo.center();


  // on positionne le servomoteur à la position spécifiée
  //servo.to( 0 );

});
