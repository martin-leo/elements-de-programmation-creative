# Johnny Five : photorésistance

Cet exemple démontre l'utilisation d'une photorésistance.

## éléments :

* une photorésistance
* une résistance de 1000 ohms

> Pourquoi une résistance de 1000 ohms ? C'est directement repris de l'[exemple](http://johnny-five.io/examples/photoresistor/) sur johnny-five.io

## branchements :

![](schema.svg)

```

  microcontrôleur
  —┬—————┬—————┬—
  gnd    +    pin
   │     |     │
   │     │     │
   │     │     │
   │     │     │
  —┴—————┴—————┴—
       servo

```

Le + peut-être du 5V.

## notes

> ne pas oublier de lancer la commande npm install ou de dupliquer le dossier node_modules avant de lancer le programme
