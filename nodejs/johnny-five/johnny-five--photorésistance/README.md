# Johnny Five : photorésistance

Cet exemple démontre l'utilisation d'une photorésistance.

## éléments :

* une photorésistance
* une résistance de 10KΩ

> Pourquoi une résistance de 10KΩ ? C'est directement repris de l'[exemple](https://playground.arduino.cc/Learning/PhotoResistor) sur arduino.cc

## branchements :

![](schema.svg)

```

  microcontrôleur
  —┬—————┬—————┬—
   +   gnd    pin
   │     |     │
   │     └—▬▬——┤
   │      10KΩ │
   │           │
  —┴———————————┴—
  photorésistance

```

## notes

> ne pas oublier de lancer la commande npm install ou de dupliquer le dossier node_modules avant de lancer le programme
