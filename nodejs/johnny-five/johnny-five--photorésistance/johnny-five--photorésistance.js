// on intègre la librairie johnny-five
var five = require("johnny-five");

// on créé un objet carte
var board = new five.Board();

// on va par exemple brancher notre photoresistance sur le pin analogique A2;
const pin_photoresistance = 'A2'; // pas le pin 2 ! attention !

// lorsque l'objet carte déclenche l'événement "prêt" (on s'est connecté à la carte avec succès)
board.on( "ready", function () {


  // Create a new `photoresistor` hardware instance.
  var photoresistance = new five.Sensor({
    pin: pin_photoresistance,
    freq: 250
  });

  // lorsque l'interrupteur déclenche l'événement "données"
  photoresistance.on ( 'data', function () {

    //on imprime le résultat mesuré
    ecrire_sur_une_ligne ( this.value );
  });

});

function ecrire_sur_une_ligne ( ce_qu_on_veut_ecrire ) {
  /* permet d'imprimer de l'information toujours sur la dernière ligne
  String -> Void */

  // on efface la ligne actuelle
  process.stdout.clearLine();

  // on se met au tout début de la ligne
  process.stdout.cursorTo(0);

  // on imprime ce qu'on veut écrire
  // le '' + permet de convertir l'entrée (Number) en chaîne de caractère (String)
  // process.stdout.write ne pouvant imrpimer des Number
  process.stdout.write( '' + ce_qu_on_veut_ecrire );
}
