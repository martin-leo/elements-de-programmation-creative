# Qu'est-ce que johnny-five ?

johnny-five est un outil qui s'intègre à l'environnement nodejs. Celui-ci permet d'interagir avec des cartes comme [Arduino](https://arduino.cc), lesquelles sont branchées sur l'ordinateur par un câble usb.

## Comment ça marche ?

On utilise une carte sur laquelle est installé un micrologiciel qui va permettre la communication entre la carte et l'ordinateur, connectés en usb, grâce au protocole [Firmata](https://www.arduino.cc/en/Reference/Firmata).

La carte est donc autonome, avec son propre programme qui ne change pas, et l'on va exécuter notre programme sur un ordinateur qui va piloter la carte.
