// on intègre la librairie johnny-five
var five = require("johnny-five");

// on créé un objet carte
var board = new five.Board();

// la diode électroluminescente intégrée est connecté au pin 13 sur une Arduino Uno
const pin_diode_electroluminescente = 13;

// on va par exemple brancher notre interrupteur sur le pin 2;
const pin_interrupteur = 2;

// lorsque l'objet carte déclenche l'événement "prêt" (on s'est connecté à la carte avec succès)
board.on( "ready", function () {

  // on va créer un objet Led (diode électroluminescente) et on lui donne comme argument le pin correspondant
  var diode_electroluminescente = new five.Led ( pin_diode_electroluminescente );

  // on va créer un objet Switch (interrupteur) et on lui donne comme argument le pin correspondant
  var interrupteur = new five.Switch( pin_interrupteur );

  // lorsque l'interrupteur déclenche l'événement "circuit ouvert"
  interrupteur.on ( 'open', function () {

    // on éteint la diode électroluminescente
    diode_electroluminescente.off();

    // on affiche un message
    console.log( 'le circuit vient d\'être ouvert' );
  });


  // lorsque l'interrupteur déclenche l'événement "circuit fermé"
  interrupteur.on ( 'close', function () {

    // on allume la diode électroluminescente
    diode_electroluminescente.on();

    // on affiche un message
    console.log( 'le circuit vient d\'être fermé' );
  });
});
