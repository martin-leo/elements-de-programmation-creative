# Johnny Five : interrupteur

Cet exemple démontre l'utilisation d'un interrupteur.

## éléments :

* un interrupteur
* une résistance de 10KΩ

> La résistance est une résistance de rappel (*pull down*). Elle évite que le pin d'entrée ne soit isolé, car les perturbations électromagnétiques pourraient le mettre ponctuellement à l'état haut, causant un bruit indésirable. Pourquoi une résistance de 10KΩ en particulier ? C'est repris de nombreux exemples qui ne le justifient pas plus que ça.

## branchements :

![](schema.svg)

```

  microcontrôleur
  —┬—————┬—————┬—
  +5v   gnd   pin
   │     |     │
   │     └—▬▬——┤
   │      10KΩ │
   │           │
  —┴———————————┴—
   interrupteur

```

On peut voir ici que lorsque le circuit est ouvert, le pin est connecté à ```gnd```, et lorsqu'il est fermé, à ```5v```. La résistance est placée entre le pin et ```gnd```.

## notes

> ne pas oublier de lancer la commande npm install ou de dupliquer le dossier node_modules avant de lancer le programme
