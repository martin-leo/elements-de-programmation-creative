// import du module file system pour l'accès au système de fichiers
const fs = require('fs');
// import du module util qui contient le module inspect qui va nous permettre d'exporter en JSON
const util = require('util');

// nom du fichier cible
const fichier_cible = "data.json";
// un objet à exporter
const objet_a_exporter = { "clé" : "valeur" };

fs.writeFile(fichier_cible, util.inspect(objet_a_exporter), (erreur) => {
  if (erreur) throw erreur;
  console.log('objet exporté en JSON');
});
