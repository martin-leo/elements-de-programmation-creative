# Enregistrer un objet en JSON

Il peut être utile, notamment lors d'une phase de debugage ou de développement, de pouvoir exporter un objet au format JSON. Vous trouverez ici utilisant util.inspect.

Contrairement à JSON.stringify, util.inspect permet de gérer les structures circulaires.
