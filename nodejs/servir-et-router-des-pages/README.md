# Router et servir des pages avec nodeJS et Express.js

Cet exemple montre comment servir des pages web statiques à l'aide de nodeJS et Express.js

## Prérequis

* nodeJS
* un navigateur (ex. : Mozilla Firefox)

### librairies utilisées

> note : les librairies node sont téléchargées automatiquement lorsque la commande **npm install** est lancée, nulle besoin de les télécharger manuellement.

* la librairie express.js

## Lancement de l'exemple

Dans un terminal, placez-vous dans le répertoire servir-et-router-des-pages, puis entrez :

```bash
npm install
```

Suivi de :

```bash
node serveur.js
```

## structure

* **serveur.js** : fichier contenant le programme principal
* **package.json** : metadonnées de notre application
* **assets/** : on va placer ici les dossiers contenant le contenu statique : html, images, css, javascript...
* **node_modules/** : ce dossier sera créé via **npm install** et contiendra les plugins node nécéssaires (**express**)
