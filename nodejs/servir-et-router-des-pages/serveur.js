// On charge la librairie express.js
var express = require('express');

// on créé un instance express.js
var app = express();

//on indique à express que l'on va aller chercher des fichiers statiques dans /assets
app.use(express.static(__dirname + '/assets'));

// En cas d'appel à la racine, on charge
// le template index
app.get('/', function(req, res) {

  // on imprime dans le terminal de quelle requête il s'agit
  console.log('Requête sur la racine');

  // on rend et renvoie le template index
  res.sendFile(__dirname + '/assets/html/index.html');
})

// si on a une autre requête que la racine
app.get('*', function(req, res) {

  // on imprime dans le terminal de quelle requête il s'agit
  console.log('Requête d\'un autre élément que la racine');

    // on envoie la page autre_page
    res.sendFile(__dirname + '/assets/html/autre_page.html');
})

// on créé le serveur
var server = app.listen(3000, function() {

  // on affiche un petit message dans le terminal
  console.log('Listening on port 3000.');
})
