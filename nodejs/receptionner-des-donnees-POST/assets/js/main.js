console.info("fichier javascript correctement chargé et exécuté");

function envoyer_donnees(data) {
  /* Créé une requête XMLHttpRequest contenant un objet transmis en paramètre converti en JSON
     Object -> Void */
  var requete = new XMLHttpRequest();

  // on converti l'objet en entrée en JSON sous forme de chaîne de caractère
  data = JSON.stringify(data);

  // On créé un écouteur d'événement pour load
  // (déclenché si les données sont correctement envoyées)
  requete.addEventListener('load', function(event) {
    console.info( 'Requête XMLHttpRequest POST correctement envoyées' );
    console.info( "Réponse du serveur : ", requete.responseText.message );
  });

  // On créé un écouteur d'événement pour error
  // (déclenché si une erreur est rencontrée)
  requete.addEventListener('error', function(event) {
    console.error('erreur lors de la requête XMLHttpRequest.');
  });

  // On met en place notre requête
  requete.open( 'POST', '/' );

  // On renseigne les en-têtes
  // type de contenu : json
  requete.setRequestHeader('Content-Type', 'application/json');
  // longueur de la requête
  requete.setRequestHeader('Content-Length', data.length);

  // On envoie les données converties en JSON
  requete.send( data );
}

document.getElementById("body").addEventListener("click", function( event ) {
  /* Écouteur d'événement "clic" sur l'élément ayant l'id body (ici la balise body), appelant une fonction en lui passant la représentation objet dudit événement en paramètre
  Event Object -> Void */

    // on créé un objet contenant différentes propriétés
    var message = {
      window_inner_width: window.innerWidth, // la largeur de la fenêtre
      window_inner_height: window.innerHeight, // la hauteur de la fenêtre
      client_x: event.clientX, // la position en x à laquelle l'événement à été enregistrée
      client_y: event.clientY, // la position en y à laquelle l'événement à été enregistré
    };

    // on passe cet objet à la fonction envoyer_donnees
    envoyer_donnees( message );
  }, false);
