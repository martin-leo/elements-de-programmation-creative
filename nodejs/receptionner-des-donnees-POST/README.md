# Récupérer des données POST avec nodejs et express.js

Cet exemple montre comment récupérer des données POST, ici les dimensions internes de la fenêtre du navigateur, ainsi que les coordonnées en x et y ou l'événement clic a été déclenché, lequel déclenche l'envoi des données.

## Prérequis

* nodeJS
* un navigateur (ex. : Mozilla Firefox)

### librairies utilisées

> note : les librairies node sont téléchargées automatiquement lorsque la commande **npm install** est lancée, nulle besoin de les télécharger manuellement.

* la librairie express.js
* la librairie body-parser (parsing des données POST)

## Lancement de l'exemple

Dans un terminal, placez-vous dans le répertoire servir-et-router-des-pages, puis entrez :

```bash
npm install
```

Suivi de :

```bash
node serveur.js
```

## structure

* **serveur.js** : fichier contenant le programme principal
* **package.json** : metadonnées de notre application
* **assets/** : on va placer ici les dossiers contenant le contenu statique : html, images, css, javascript... Et notamment :
  * **assets/js/main.js** : le programme côté client (navigateur)
* **node_modules/** : ce dossier sera créé via **npm install** et contiendra les plugins node nécéssaires (**express**)
* JavaScript
