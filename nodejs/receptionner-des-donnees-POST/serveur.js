// on charge le module util qui contient inspect qui va nous permettre de convertir des objets en JSON
const util = require('util');

// On charge la librairie express.js
var express = require('express');

// on charge la librairie body-parser qui va nous permettre de lire le contenu des requêtes POST
var bodyParser = require('body-parser');

// on créé un instance express.js
var app = express();

// on créé une référence au parser application/json
var parser_json = bodyParser.json()

// si l'on souhaitait parser du application/x-www-form-urlencoded
// on pourrait créer comme suit un parser urlencoded
// var parser_urlencoded = bodyParser.urlencoded({ extended: false });

//on indique à express que l'on va aller chercher des fichiers statiques dans /assets
app.use(express.static(__dirname + '/assets'));

// En cas d'appel à la racine,
// on charge le template index
app.get('/', function(req, res) {

  // on imprime dans le terminal de quelle requête il s'agit
  console.log('Requête sur la racine');

  // on envoie la page index
  res.sendFile(__dirname + '/assets/html/index.html');
})

// si on a une autre requête que la racine
app.get('*', function(req, res) {

  // on imprime dans le terminal de quelle requête il s'agit
  console.log('Requête d\'un autre élément que la racine');

  // on envoie la page autre_page
  res.sendFile(__dirname + '/assets/html/autre_page.html');
})

// si l'on reçoit une requête POST sur la racine
// on utilise le parser JSON
app.post('/', parser_json, function (req, res) {
  
  // on peut renvoyer un objet en retour
  res.send('{"code":100,"message":"données reçues"}');

  // et afficher ce que reçu
  console.log('données POST reçues');
  console.log(util.inspect(req.body))
})

// on créé le serveur qui va couter le port 3000
var server = app.listen(3000, function() {

  // on affiche un petit message dans le terminal
  console.log('Listening on port 3000.');
})
