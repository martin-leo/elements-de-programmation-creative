// on créé une occurence de socket.io
var socket = io();

// on cible le body grâce à son id body
var body = document.getElementById('body');

// on lui attache un écouteur d'événement clic
body.addEventListener('click', function(event) {

  // on créé un objet qui contient les dimensions de la fenêtre,
  // les coordonnées du clic
  var message = {
    window_inner_width: window.innerWidth, // la largeur de la fenêtre
    window_inner_height: window.innerHeight, // la hauteur de la fenêtre
    client_x: event.clientX, // la position en x à laquelle l'événement à été enregistrée
    client_y: event.clientY, // la position en y à laquelle l'événement à été enregistré
  };

  // on l'envoie
  socket.emit('message-exemple-websockets', message);
}, false);

// lorsque l'on reçoit un événement 'message-exemple-websockets'
socket.on('message-exemple-websockets', function(message){

  // si la propriété emetteur de l'objet reçu est identiqué à l'UUID du client
  if ( message.emetteur !== socket.io.engine.id ) {

    // on affiche l'objet dans la console
    console.log(message);

  // dans le cas contraire (on reçoit un message que l'on a envoyé)
  } else {

    // on affiche un simple message dans la console
    console.info('reçu : broadcast du message envoyé')
  }
});
