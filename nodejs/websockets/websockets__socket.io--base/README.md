# Broadcaster une information en utilisant les websocket

Cet exemple montre comment broadcaster une information via websocket.

## Requis

* node.js
* express.js
* socket.io

## Lancement de l'exemple

Dans un terminal, placez-vous dans le répertoire websockets__socket.io--base, puis entrez :

```bash
npm install
```

Suivi de :

```bash
node serveur.js
```

## structure

* **serveur.js** : fichier contenant le programme principal
* **package.json** : metadonnées de notre application
* **assets/** : on va placer ici les dossiers contenant le contenu statique : html, images, css, javascript... Dont :
  * **assets/js/main.js** : Le programme principal côté client.
* **node_modules/** : ce dossier sera créé via **npm install** et contiendra les plugins node nécéssaires (**express**)
