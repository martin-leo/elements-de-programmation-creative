// import du module util qui contient le module inspect qui va nous permettre d'exporter en JSON
const util = require('util');

// On charge la librairie express.js
var express = require('express');

// on créé un instance express.js
var app = express();

// on créé le serveur
var server = app.listen(3000, function() {

  // on affiche un petit message dans le terminal
  console.log('Listening on port 3000.');
})

// on créé le serveur socket.io et on l'attache à notre serveur express
var io = require('socket.io')(server);

// on indique de renvoyer les requêtes de contenu statique vers /assets
app.use('/', express.static(__dirname + '/assets'));

// lors des requêtes GET sur la racine, on envoie le fichier index.html
app.get('/', function(req, res){
  res.sendFile(__dirname + '/assets/html/index.html');
});
// lors des requêtes GET sur /autre on envoie aussi index.html
// mais on pourrait envoyer autre chose
app.get('/autre', function(req, res){
  res.sendFile(__dirname + '/assets/html/index.html');
});

// à chaque connection (socket) créé
io.on('connection', function(socket){

  // on associe un écouteur d'événement
  socket.on('message-exemple-websockets', function(message){

    // on ajoute au message l'id de l'emetteur
    message.emetteur = socket.id;

    // on 'broadcast' le message à tout le monde
    io.emit('message-exemple-websockets', message);

    // on affiche dans le terminal ce qu'on vient de broadcaster
    console.log(util.inspect(message));
  });
});
