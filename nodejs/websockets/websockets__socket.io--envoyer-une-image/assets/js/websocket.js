// on créé une occurence de socket.io
var socket = io();

// on crée une fonction pour l'envoi d'image
function envoyer_image () {

  // on cible le canvas qui contient (l'image à envoyer)
  var canvas = document.getElementById('canvas');

  // on créé un objet qui contient l'image au format chaîne de caractères,
  var message = {
    image: canvas.toDataURL('image/jpeg', 1.0),
  }

  // on l'envoie
  socket.emit('message-exemple-image', message);
}

// lorsque l'on reçoit un événement 'message-exemple-image'
socket.on('message-exemple-image', function(message){

  // si la propriété emetteur de l'objet reçu est identiqué à l'UUID du client
  if ( message.emetteur !== socket.io.engine.id ) {

    // on affiche l'image sur la page
    afficher_image_reçue(message.image);

  // dans le cas contraire (on reçoit un message que l'on a envoyé)
  } else {

    // on affiche un simple message dans la console
    console.info('reçu : broadcast du message envoyé')
  }
});

function afficher_image_reçue (image_base64) {
  /* Créé un élément image avec l'image encodée sous forme d'url comme paramètre
     et le place dans la page */

    // élément cible où l'on va placer nos images
    var element_cible = document.getElementById('zone--reception');

    // élément image
    var nouvelle_image = document.createElement('img');
    nouvelle_image.setAttribute('src', image_base64);

    // on place celui-ci dans l'élément cible
    element_cible.appendChild(nouvelle_image);
}
