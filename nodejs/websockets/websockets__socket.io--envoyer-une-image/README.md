# Envoyer une image fixe capturée par webcam via websocket

Cet exemple montre comment :

* capturer une image fixe issu d'une webcam
* l'envoyer sous forme de chaîne de caractère via websocket

## Requis

* node.js
* express.js
* socket.io
## Lancement de l'exemple

Dans un terminal, placez-vous dans le répertoire websockets__socket.io--envoyer-une-image, puis entrez :

```bash
npm install
```

Suivi de :

```bash
node serveur.js
```

## structure

* **serveur.js** : fichier contenant le programme principal
* **package.json** : metadonnées de notre application
* **assets/** : on va placer ici les dossiers contenant le contenu statique : html, images, css, javascript... Dont :
  * **assets/js/video.js** : Les instructions relatives à la vidéo.
  * **assets/js/websockets.js** : Les instructions relatives à la communication via websocket.
* **node_modules/** : ce dossier sera créé via **npm install** et contiendra les plugins node nécéssaires (**express**)
