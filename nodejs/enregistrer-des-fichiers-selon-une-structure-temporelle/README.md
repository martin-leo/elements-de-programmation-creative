# Enregistrer des fichiers selon une structure temporelle

On peut avoir besoin d'enregistrer des informations tout en gardant la notion du moment où on les à sauvegardées.

Cet exemple présente une fonction ```sauvegarder( objet )``` qui enregistre un objet au format json dans un fichier dont le chemin sera ```exports/AAAA/MM/JJ/H-M-S-millis.json``` où ```AAAA``` : année, ```MM``` : mois, ```JJ``` : jour, ```H``` : heure, ```M``` : minute, ```S``` : seconde et ```millis``` : millisecondes.
