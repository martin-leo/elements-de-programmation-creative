// import du module file system pour l'accès au système de fichiers
const fs = require('fs');

// import du module util qui contient le module inspect qui va nous permettre d'exporter en JSON
const util = require('util');

// un objet à exporter
const objet_a_exporter = { "clé" : "valeur" };

sauvegarder( objet_a_exporter );

function sauvegarder (objet) {
  /* sauvegarde un message sur le disque */

  /* Constante de temps pour l'enregistrement */

  const temps = new Date();
  const AAAA = temps.getFullYear();
  const MM = temps.getMonth() + 1; // mois de 0 à 11, on ajoute 1
  const DD = temps.getDate();
  const H = temps.getHours();
  const M = temps.getMinutes();
  const S = temps.getSeconds();
  const millis = temps.getMilliseconds();

  /* création de la structure de stockage : exports/AAAA/MM/DD/H-M-S-millis.json */

  // chemin du fichier de sauvgarde
  const fichier_cible = 'exports/' + AAAA + '/' + MM + '/' + DD + '/' + H + '-' + M + '-' + S + '-' + millis ;

  // dossier /exports/
  if ( ! fs.existsSync( 'exports') ) {
    fs.mkdirSync( 'exports' );
  }

  // dossier exports/AAAA/
  if ( ! fs.existsSync( 'exports/' + AAAA ) ) {
    fs.mkdirSync( 'exports/' + AAAA );
  }

  // dossier exports/AAAA/MM/
  if ( ! fs.existsSync( 'exports/' + AAAA + '/' + MM ) ) {
    fs.mkdirSync( 'exports/' + AAAA + '/' + MM );
  }

  // dossier exports/AAAA/MM/DD/
  if ( ! fs.existsSync( 'exports/' + AAAA + '/' + MM + '/' + DD ) ) {
    fs.mkdirSync( 'exports/' + AAAA + '/' + MM + '/' + DD );
  }

  // on passe audit enregistrement
  fs.writeFile(fichier_cible, util.inspect(objet), (erreur) => {
    if (erreur) {
      console.error( 'erreur lors de la tentative d\'écriture de ' + fichier_cible );
    };
    console.log( 'l\'écriture de ' + fichier_cible + ' s\'est correctement déroulée' );
  });

}
