/* contrôle d'un module MOSFET

Ce programme montre comment programmer un module MOSFET
afin que celui-ci laisse ou ne laisse pas passer le courant
toutes les 5 secondes

  schéma :

  alimentation    élément cible
    —┬———┬—          —┬———┬—
     +   -            +   -
     │   └——┐     ┌———┘   │
     └———┐  │     │  ┌————┘
         │  │     │  │
       vin  gnd   v+ v-
        —┴——┴—————┴——┴—
         module MOSFET
        —┬—————┬—————┬—
        gnd   vcc   sig(nal)
         │     │     │
        gnd   +5v   pin
         │     │     │
        —┴—————┴—————┴—
        microcontrôleur

*/

// le pin qui va nous servir à contrôler le module
int pin_de_controle = 10;

// la fonction void qui sera exécutée une seule fois, en tout premier
void setup()
{
  pinMode(pin_de_controle, OUTPUT);
}

// la fonction loop qui sera exécutée en boucle, après setup
void loop()
{
  // pin de contrôle à +5V : le MOSFET laisse passer le courant
  digitalWrite(pin_de_controle, HIGH);

  // on attend 5 secondes (5000 millisecondes)
  delay(5000);

  // pin de contrôle à 0V : le MOSFET ne laisse pas passer le courant
  digitalWrite(pin_de_controle, LOW);

  // on attend 5 secondes (5000 millisecondes)
  delay(5000);
}
