# Commuter un circuit alimenté en 0 à 24V

S'il est possible d'alimenter et piloter directement des éléments en les branchant sur un microcontrôleur, cela à malheureusement ses limites : l'ampérage (<= 40 mA pour une carte Arduino) et le voltage de la carte (5V sur une carte Arduino).

Pour palier à cela, on peut utiliser, pour de faibles tensions, un module MOSFET.

## Photographie d'un module MOSFET

L'aspect et la connectique peuvent changer selon le fabricant.

![](module-mosfet.png)

## Schéma

```
  alimentation    élément cible
    —┬———┬—          —┬———┬—
     +   -            +   -
     │   └——┐     ┌———┘   │
     └———┐  │     │  ┌————┘
         │  │     │  │
       vin  gnd   v+ v-
        —┴——┴—————┴——┴—
         module MOSFET
        —┬—————┬—————┬—
        gnd   vcc   sig(nal)
         │     │     │
        gnd   +5v   pin
         │     │     │
        —┴—————┴—————┴—
        microcontrôleur
```

## exemple de programme

Toutes les 5 secondes, on allume ou éteint le courant.

```c
// le pin qui va nous servir à contrôler le module
int pin_de_controle = 10;

// la fonction void qui sera exécutée une seule fois, en tout premier
void setup()
{
  pinMode(pin_de_controle, OUTPUT);
}

// la fonction loop qui sera exécutée en boucle, après setup
void loop()
{
  // pin de contrôle à +5V : le MOSFET laisse passer le courant
  digitalWrite(pin_de_controle, HIGH);

  // on attend 5 secondes (5000 millisecondes)
  delay(5000);

  // pin de contrôle à 0V : le MOSFET ne laisse pas passer le courant
  digitalWrite(pin_de_controle, LOW);

  // on attend 5 secondes (5000 millisecondes)
  delay(5000);
}
```
