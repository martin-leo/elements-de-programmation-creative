#include <ArduinoJson.h>

// Cet exemple est tiré de https://arduinojson.org/doc/decoding/
// il utilise la librairie ArduinoJson
// vous trouverez aussi de l'aide sur https://arduinojson.org/assistant/
// notamment du code prêt à l'emploi

// JSON à parser
char entree[] = "{\"chaine\":\"valeur\",\"nombre\":128,\"tableau\":[3.14,10.12]}";

// calcule de la capacité mémoire nécéssaire pour un objet avec 3 propriétés plus un tableau plus
const int capacite_memoire_allouee = JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(2) + 50;

// on peut aussi utiliser https://arduinojson.org/assistant/
// et spécifier cette valeur « en dur »
// const int capacite_memoire_allouee = 200;

// buffer dynamique pour stocker notre objet issu du JSON
DynamicJsonBuffer buffer_json ( capacite_memoire_allouee );

// pour une carte avec une mémoire limité on préférera un staticBuffer
// StaticJsonBuffer <capacite_memoire_allouee> buffer_json;

void setup() {
  /* Cette fonction est exécutée en tout premier, une seule fois */

  // petit délai optionnel pour avoir le temps d'ouvrir le moniteur série
  delay( 2500 );

  // début de la liaison série, 115200 baud/s
  Serial.begin( 115200 );

  // on traite notre JSON
  traitement_json( entree );
}
 
void loop() {
  /* fonction exécutée en boucle, ici vide */
}

void traitement_json ( char json[] ) {
  /* traite du JSON en entrée
  Tableau de caractère -> Void */
  
  // la fonction parseobject nous renvoie une référence
  // à un JsonObject situé dans notre buffer, et non une copie
  JsonObject& objet_en_sortie = buffer_json.parseObject( json );
  
  // on teste si le parsing a réussi
  if ( objet_en_sortie.success() ) {
    
    Serial.println( "succès du parsing" );
    
    const char* chaine = objet_en_sortie["chaine"]; // "valeur"
    int nombre = objet_en_sortie["nombre"]; // 128
    float tableau_0 = objet_en_sortie["tableau"][0]; // 3.14
    float tableau_1 = objet_en_sortie["tableau"][1]; // 10.12
    Serial.println( chaine );
    Serial.println( nombre );
    Serial.println( tableau_0 );
    Serial.println( tableau_1 );
  
  } else {
    
    // en cas d'échec, 3 possibilités
    // * JSON non valide
    // * buffer trop petit
    // * pas assez de mémoire
    Serial.println( "échec du parsing" );
  }
}
