# utilisation de la carte wemos d1 mini avec l'IDE Arduino

Une [documentation de démarrage](https://wiki.wemos.cc/tutorials:get_started:get_started_in_arduino) est disponible sur le site du fabricant.

## installation des pilotes pour le composant CH340

Cette carte est basé sur le composant CH340 pour l'USB, qui est nativement supporté par Linux mais pas par macOS ou ms-windows. Le fabricant met des pilotes à disposition [sur son site](http://www.wch.cn/products/CH340.html).

* [lien pour le pilote mac](http://www.wch.cn/download/CH341SER_EXE.html)
* [lien pour le pilote ms-windows](http://www.wch.cn/download/CH341SER_MAC_ZIP.html)

## Mettre à jour le gestionnaire de carte additionnelles

Dans les préférences, ajouter l'URL suivante à la liste d'URL du gestionnaire de cartes supplémentaires :

```http://arduino.esp8266.com/stable/package_esp8266com_index.json```
