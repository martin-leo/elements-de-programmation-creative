#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

// notre client HTTP
HTTPClient client_http;

// cet exemple est tiré de l'exemple BasicHttpClient de la librairie ESP8266HTTPClient.

// -----------------------------------------

// informations protocole, serveur, port, URL
const char* protocole = "http";
const char* adresse_serveur = "192.168.1.26";
const char* port = "8080";
const char* chemin = "/data.json";

// -----------------------------------------

// informations pour la connexion au WiFi

// SSID du réseau
const char* ssid = "nom-du-SSID";

// mot de passe
const char* mot_de_passe = "mot-de-passe";

// hostname
const char* nom_d_hote = "wemos 1d mini";

// -----------------------------------------

void setup() {
  /* Cette fonction est exécutée en tout premier, une seule fois */

  // début de la liaison série, 115200 baud/s
  Serial.begin( 115200 );

  // on attend 10 ms
  delay(10);

  // procédure de connexion au WiFi définie plus bas
  connexion_WiFi();
  
  String url_requete = protocole + String("://") + adresse_serveur + String(":") + port + chemin;

  // requête GET visant à obtenir notre JSON (définie plus bas)
  requete( url_requete );
}
 
void loop() {
  /* fonction exécutée en boucle, ici vide */
}

void connexion_WiFi () {
  /* Connexion au wifi */
  
  Serial.print( "Connexion à ");
  Serial.println ( ssid );
  WiFi.hostname( nom_d_hote );
  WiFi.begin( ssid, mot_de_passe );

  // tant que le WiFi n'est pas connecté
  while ( WiFi.status() != WL_CONNECTED ) {

    // on patiente une demi-seconde
    delay(500);

    // on affiche des points
    Serial.print("•");
  }

  // si on n'est plus « pas connecté » : on est connecté et on l'affiche
  Serial.println(); // saut de ligne
  Serial.println("Connexion au WiFi réussie");
 
  // On imprime l'adresse IP locale
  Serial.print( "Adresse IP locale : " );
  Serial.println( WiFi.localIP() );
 }

void requete ( String url_requete ) {
  /* formule une requete GET avec les informations fournies et affiche le résultat de celle-ci */

  // on affiche des informations sur la requête
  Serial.println(); // saut de ligne
  Serial.print ( "Requête sur : " );
  Serial.println ( url_requete );

  // on lance la requête
  client_http.begin ( url_requete );

  // on récupère le code HTTP (négatif si erreur)
  int code_http = client_http.GET();

  // si on a pas d'erreur
  if ( code_http > 0 ) {

    // on affiche le code HTTP
    Serial.print ( "Code HTTP suite à la requête GET : " );
    Serial.println ( code_http );
  
    // si fichier trouvé
    if( code_http == HTTP_CODE_OK ) {

      // on récupère le contenu
      String reponse = client_http.getString();

      // et on l'affiche
      Serial.print ( "Réponse à la requête GET : " );
      Serial.println ( reponse );
    }

  // si on a une erreur
  } else {

    // on l'affiche
    Serial.print ( "Erreur HTTP : ");
    Serial.println ( client_http.errorToString( code_http ).c_str() );
  }

  // on a terminé : on met fin à la requête
  client_http.end();
}
