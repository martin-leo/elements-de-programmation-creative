#include <ESP8266WiFi.h>

// SSID du réseau
const char* ssid = "nom-du-SSID";

// mot de passe
const char* mot_de_passe = "mot-de-passe";

// hostname
const char* nom_d_hote = "wemos d1 mini";
 
void setup() {
  /* Cette fonction est exécutée en tout premier, une seule fois */

  // début de la liaison série, 115200 baud/s
  Serial.begin( 115200 );

  // on attend 10 ms
  delay(10);

  // on lance la procédure de connexion définie plus bas
  connexion_WiFi();
}
 
void loop() {
  /* fonction exécutée en boucle, ici vide */
}

void connexion_WiFi () {
  /* Connexion au wifi */

  // on imprime le SSID
  Serial.print( "Connexion à ");
  Serial.println ( ssid );

  // on paramètre le hostname de la carte
  WiFi.hostname( nom_d_hote );

  // on démarre la liaison WiFi
  WiFi.begin( ssid, mot_de_passe );

  // tant que le WiFi n'est pas connecté
  while ( WiFi.status() != WL_CONNECTED ) {

    // on patiente une demi-seconde
    delay(500);

    // on affiche des points
    Serial.print("•");
  }

  // si on n'est plus « pas connecté » : on est connecté et on l'affiche
  Serial.println(); // saut de ligne
  Serial.println("Connexion au WiFi réussie");
 
  // On imprime l'adresse IP locale
  Serial.print( "Adresse IP locale : " );
  Serial.println( WiFi.localIP() );
 }
