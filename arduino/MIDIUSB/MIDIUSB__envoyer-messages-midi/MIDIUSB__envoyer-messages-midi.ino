/*
  MIDIUSB__envoyer-messages-midi.ino

  Ce code est utilisable sur les cartes avec capacité USB native,
  comme l'arduino leonardo ou pro micro.
  Pour commencer, vous devez installer la librairie MIDIUSB,
  pour cela, cliquez, dans le menu,
  sur croquis → inclure une bibliothèque → gérer les bibliothèques.
  recherchez alors 'midiusb' → installer

  On peut envoyer principalement deux types de messages MIDI :
  1. note-on/note-off : correspond à si une note est appuyée/relâchée
  2. Controle-change : correspond à des modifications de paramètres (volume, effets, etc.)

  Il existe d'autres messages qui ne seront pas couverts ici.

  Un message note-on ou note-off comporte plusieurs variables :
  1. le canal, de 0 à 15 (16 canaux donc)
  2. la hauteur de note, de 0 à 127 (128 valeurs)
  3. la vélocité avec laquelle la note est appuyée/relâchée, de 0 à 127 (128 valeurs)

  Un message controle-change comporte aussi plusieurs variables :
  1. le canal, de 0 à 15 (16 canaux donc)
  2. le type de paramètre associé (volume, balance, effets, etc.)
  3. la valeur modifiée du paramètre, de 0 à 127 (128 valeurs)

  À noter que pour le type de paramètre associé,
  la bibliothèque indique des valeurs de 0 à 119,
  tandis que la page
  https://fr.audiofanzine.com/mao/editorial/dossiers/le-midi-les-midi-control-change.html
  indique des valeurs de 0 à 101 puis de 120 à 127

*/

// on inclue la librairie MIDIUSB
#include "MIDIUSB.h"

// Fonctions utiles que l'on va pouvoir réutiliser plus tard

void note_on ( byte canal, byte hauteur, byte velocite ) {
  /* Ajoute un message de démarrage d'une note à la pile de messages en attente
  renseigne le canal (0-15), la hauteur (0-127), la vélocité (0-127) */

  // on créé une variable de type midiEventPacket_t
  // avec type (note-on = 0x09, type + canal, hauteur, vélocité
  midiEventPacket_t message = { 0x09, 0x90 | canal, hauteur, velocite };

  // on place la note en attente d'envoi avec MidiUSB.sendMIDI
  MidiUSB.sendMIDI( message );
}

void note_off ( byte canal, byte hauteur, byte velocite ) {
  /* Ajoute un message de relâchement d'une note à la pile de messages en attente
  renseigne le canal (0-15), la hauteur (0-127), la vélocité (0-127) */

  // on créé une variable de type midiEventPacket_t
  // avec type (note-on = 0x08, type + canal, hauteur, vélocité

  // on place la note en attente d'envoi avec MidiUSB.sendMIDI
  midiEventPacket_t message = { 0x08, 0x80 | canal, hauteur, velocite };
  MidiUSB.sendMIDI( message );
}

void control_change ( byte canal, byte type, byte valeur ) {
  /* Ajoute un message de changement de paramètre à la pile de messages en attente
  renseigne le canal (0-15), le type, la valeur (0-127) */
  midiEventPacket_t message = { 0x0B, 0xB0 | canal, type, valeur };

  // on place la note en attente d'envoi avec MidiUSB.sendMIDI
  MidiUSB.sendMIDI( message );
}

// Les fonction habituelles à définir et qui seront exécutées après lecture du programme

void setup() {
  /* définition de la fonction qui sera appelée une unique fois,
  et en tout premier lors de l'exécution du programme */

  // on démarre la liaison série
  Serial.begin( 115200 );
}

void loop() {
  /* définition de la fonction qui sera appelée en boucle,
  un fois la fonction setup terminée */

  Serial.println( "envoi d'un message MIDI note on" );

  // on met en attente notre message, canal 0, hauteur 48/127, vélocité 64/127
  note_on( 0, 48, 64 );

  // on envoie tous les messages en attente
  MidiUSB.flush();

  // on attend 500 ms
  delay( 500 );


  Serial.println( "envoi d'un message MIDI note on" );

  // Channel 0, middle C, normal velocity
  note_off( 0, 48, 64 );

  // on envoie tous les messages en attentes
  MidiUSB.flush();
  delay(2000); // on attend 2000 ms (2s)

  // on va envoyer une suite de message de à 127 puis de 127 à zéro
  // pour cela, on fait deux boucles
  // la première pour monter le volume
  for ( int i = 0; i <= 127; i++ ) {

    // canal 0, volume, i
    controlChange( 0, 7, i );

    // on envoie
    MidiUSB.flush();

    // on attend 100ms (0.1s)
    delay(100);
  }

  // la seconde boucle pour descendre le volume
  for ( int i = 127; i >= 0; i-- ) {

    // canal 0, volume, i
    controlChange( 0, 7, i );

    // on envoie
    MidiUSB.flush();

    // on attend 100ms (0.1s)
    delay(100);
  }

  // une fois terminé, on attend à nouveau 2000ms (2s)
  delay(2000);

}
