/* Les uid lus sont saisies par la carte arduino/clavier */

// librairie pour la communication https://www.arduino.cc/en/Reference/SPI
#include <SPI.h>

// on importe la librairie https://github.com/miguelbalboa/rfid
#include <MFRC522.h>

// on importe la librairie KeyboardAzertyFr, disponible dans Croquis → Inclure une bibliothèque → Gérer les bibliothèques
// Il s'agit de l'adaptation de la librairie Keyboard pour clavier azerty français
#include <KeyboardAzertyFr.h>

#define SS_PIN 10 // pin SDA, sur pro micro = 10
#define RST_PIN 9 // pin RST, ici on prend le pin 9
MFRC522 module_rfid ( SS_PIN, RST_PIN );  // on créé une instance de mfrc522

// la fonction suivante est exécutée une fois, au lancement du programme
void setup() {
  // on démarre l'émulation d'un clavier
  KeyboardAzertyFr.begin();
  SPI.begin();          // initialisation du bus SPI
  module_rfid.PCD_Init();   // initialisation du module MFRC522
}

// la fonction suivant est exécutée en boucle, une fois setup() terminée
void loop() {
  /**/

  // on ne poursuit que si on a une nouvelle carte
  if ( ! module_rfid.PICC_IsNewCardPresent() ) {
    return;
  }

  // on ne poursuit que si on a ou en lire l'UID
  if ( ! module_rfid.PICC_ReadCardSerial() ) {
    return;
  }

  // impression d'une chaîne de caractères
  // La chaîne est la conversion de l'id sous forme de Byte vers un String
  // voir arduino.cc/en/Reference/KeyboardPrint
  KeyboardAzertyFr.print( "<" );
  for (int i = 0; i < module_rfid.uid.size; i++) {
    KeyboardAzertyFr.print( "0x" );
    KeyboardAzertyFr.print( String( module_rfid.uid.uidByte[i], HEX ) );
    if ( i < module_rfid.uid.size - 1 ) {
      KeyboardAzertyFr.print( ":" );
    }
  }
  KeyboardAzertyFr.print( ">" );

  // met le lecteur en attente tant que la carte actuelle reste (afin d'éviter qu'elle ne renvoie en boucle son signal)
  module_rfid.PICC_HaltA();
}
