/* Si l'ui' lu est associé à une chaîne de caractère, cette chaîne est saisie par la carte arduino/clavier */

// librairie pour la communication https://www.arduino.cc/en/Reference/SPI
#include <SPI.h>

// on importe la librairie https://github.com/miguelbalboa/rfid
#include <MFRC522.h>

// on importe la librairie KeyboardAzertyFr, disponible dans Croquis → Inclure une bibliothèque → Gérer les bibliothèques
// Il s'agit de l'adaptation de la librairie Keyboard pour clavier azerty français
#include "KeyboardAzertyFr.h"

#define SS_PIN 10 // pin SDA, sur pro micro = 10
#define RST_PIN 9 // pin RST, ici on prend le pin 9
MFRC522 module_rfid ( SS_PIN, RST_PIN );  // on créé une instance de mfrc522

#define NOMBRE_DE_CARTES              7
#define NOMBRE_DE_BYTES_DANS_L_ID     7

// les uid des cartes que l'on veut reconnaître
byte cartes[NOMBRE_DE_CARTES][NOMBRE_DE_BYTES_DANS_L_ID] = {
  { 0x04, 0xd5, 0xf4, 0xf2, 0x73, 0x4C, 0x80 }, // 04 D5 F4 F2 73 4C 80
  { 0x04, 0xc5, 0xf4, 0xf2, 0x73, 0x4C, 0x80 }, // 04 C5 F4 F2 73 4C 80
  { 0x04, 0xcd, 0xf4, 0xf2, 0x73, 0x4C, 0x80 },  // 04 DE F4 F2 73 4C 80
  { 0x04, 0xbd, 0xf4, 0xf2, 0x73, 0x4C, 0x80 },  // 04 BD F4 F2 73 4C 80
  { 0x04, 0xc5, 0xf4, 0xf2, 0x73, 0x4C, 0x80 },  // 04 C5 F4 F2 73 4C 80
  { 0x04, 0xd5, 0xf4, 0xf2, 0x73, 0x4C, 0x80 },  // 04 D5 F4 F2 73 4C 80
  { 0x04, 0xcd, 0xf4, 0xf2, 0x73, 0x4C, 0x80 }  // 04 CD F4 F2 73 4C 80
 };

// les chaînes de caractères que l'on veut taper au clavier
String touches_clavier[NOMBRE_DE_CARTES] = {
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6"
 };

// chaîne de caractère si carte non reconnue
String carte_non_reconnue = "x";

void setup() {
  /**/

  KeyboardAzertyFr.begin();
  SPI.begin();          // initialisation du bus SPI
  module_rfid.PCD_Init();   // initialisation du module MFRC522
}

void loop() {
  /**/

  // on ne poursuit que si on a une nouvelle carte
  if ( ! module_rfid.PICC_IsNewCardPresent() ) {
    return;
  }

  // on ne poursuit que si on a ou en lire l'UID
  if ( ! module_rfid.PICC_ReadCardSerial() ) {
    return;
  }

  int carte = tester_uid( module_rfid.uid.uidByte, module_rfid.uid.size );

  if ( carte >= 0 ) {
    KeyboardAzertyFr.print( touches_clavier[ carte ] );
  } else {
    KeyboardAzertyFr.print( carte_non_reconnue );

  }

  // met la carte rfid en pause tant qu'elle reste sur le lecteur (elle ne renvoie pas en boucle son signal)
  module_rfid.PICC_HaltA();
}

int tester_uid (byte *buffer, byte taille_buffer ) {
  /* on teste, pour toutes les cartes enregistrées dans cartes, la correspondance avec ce que lu
  on renvoie l'int correspondant à l'index de la première carte correcte, et sinon -1 */
  byte nombre_de_bytes_corrects = 0;
  // pour toutes les cartes enregistrées
  for ( int c = 0; c < NOMBRE_DE_CARTES; c++ ) {
    nombre_de_bytes_corrects = 0;
    // pour tous les bytes de l'uid de la carte
    for (byte b = 0; b < taille_buffer; b++) {
      if ( buffer[b] == cartes[c][b] ) {
        nombre_de_bytes_corrects++;
      }
    }

    // si tous les bytes étaients bons
    if ( nombre_de_bytes_corrects == NOMBRE_DE_BYTES_DANS_L_ID ) {
      return c;
    }
  }
  return -1;
}
