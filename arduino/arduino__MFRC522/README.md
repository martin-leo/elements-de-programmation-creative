# MFRC522

## câblage

[Référence](https://github.com/miguelbalboa/rfid#pin-layout)

| pin MFRC522 | pin uno | pin leonardo | pin pro micro | pin Mega2560 |
| --- |
| SDA (data) | A4 (sda) | 2 (sda) | | 20 (sda) |
| SCK (clock) | A5 (scl) | 3 (scl) | | 21 (scl) |
| MOSI | 11 | | 14 | |
| MISO | 12 | | 16 | |
| IOR | ⦰ | ⦰ | ⦰ | ⦰ |
| RST (reset) | | | | |
| 3.3V | ⦰ | ⦰ | ⦰ | ⦰ |
| 5V | 5V | 5V | VCC | 5V |
