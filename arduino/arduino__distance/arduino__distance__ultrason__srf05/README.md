# Capteur de distance à ultrason

Cet exemple montre l'usage du capteur de distance à ultrason SRF05 avec une carte arduino.

## Prérequis

* une carte arduino
* le logiciel [arduino](https://arduino.cc)
* la librairie [newPing](playground.arduino.cc/Code/NewPing)

## Installation de la librairie

Le fichier zip peut être installé en faisant Croquis -> Inclure une bibliothèque -> Ajouter la bibliothèque .ZIP.

## Composant et pins

![Schéma manquant](SRF05--photo.jpg "Schéma")

Le SRF05 comporte 10 pins dont seulement 4 seront utilisés. Cette photo permet de les repérer :
* 5V
* Echo
* Trigger
* 0V

## Schéma

![Schéma manquant](arduino__distance__ultrason__srf05_bb.svg "Schéma")

## Branchements

```
――┬―― SRF05 ――┬―― VCC  ⦁━━━━━━━━━━⦁ 5V  ――┬―― arduino
――┤           ├―― Echo ⦁━━━━━━━━━━⦁ 3~  ――┤
――┤           ├―― Trig ⦁━━━━━━━━━━⦁ 2   ――┤
――┤           ├―― Mode                   │
――┘           └―― GND  ⦁━━━━━━━━━━⦁ GND ――┘
```

## Notes

* distance min : 1 cm
* distance max : 400 cm
