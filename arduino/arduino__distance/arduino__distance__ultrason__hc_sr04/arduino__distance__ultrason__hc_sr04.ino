/*
  Ce programme permet de lire la valeur d'un télémètre ultrason hc-sr04.

  Schéma :

  hc-sr04 ――┬―― VCC  ―⦁━━━━━━━━━━⦁― 5V  ――┬―― arduino
            ├―― Trig ―⦁━━━━━━━━━━⦁― 3~  ――┤
            ├―― Echo ―⦁━━━━━━━━━━⦁― 2   ――┤
            └―― GND  ―⦁━━━━━━━━━━⦁― GND ――┘

*/
// import de la librairie NewPing
// à installer au préalable : playground.arduino.cc/Code/NewPing
#include <NewPing.h>

// on branche la sortie trigger du capteur sur le pin 3
// et la sortie echo sur le pin 2
#define TRIGGER_PIN  3
#define ECHO_PIN     2

// le capteur détecte jusqu'à 400 cm
#define MAX_DISTANCE 400

// on créé une occurence d'un objet NewPing
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

void setup() {
  /* Fonction appelée une unique fois par Arduino, mais en tout premier */
  Serial.begin(9600);  // On démarre la liaison série
}

void loop() {
  /* Fonction appelée en boucle par Processing après la fonction setup */

  // on insère un délai pour éviter de faire tourner le programme plus que nécéssaire.
  delay(50);

  // on imprime dans le moniteur série la distance captée
  // l'accès au monteur série se fait via outils ⟶ moniteur série
  Serial.print("Ping: ");
  Serial.print(sonar.ping_cm());
  Serial.println("cm");
}
