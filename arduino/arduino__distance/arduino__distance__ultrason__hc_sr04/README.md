# Capteur de distance à ultrason

Cet exemple montre l'usage du capteur de distance à ultrason HC-SR04 avec une carte arduino.

## Prérequis

* une carte arduino
* le logiciel [arduino](https://arduino.cc)
* la librairie [newPing](playground.arduino.cc/Code/NewPing)

## Installation de la librairie

Le fichier zip peut être installé en faisant Croquis -> Inclure une bibliothèque -> Ajouter la bibliothèque .ZIP.

## Schéma

![Schéma manquant](arduino__distance__ultrason__hc_sr04_bb.png "Schéma")

## Branchements

```
hc-sr04 ――┬―― VCC  ⦁━━━━━━━━━━⦁ 5V  ――┬―― arduino
          ├―― Trig ⦁━━━━━━━━━━⦁ 3~  ――┤
          ├―― Echo ⦁━━━━━━━━━━⦁ 2   ――┤
          └―― GND  ⦁━━━━━━━━━━⦁ GND ――┘
```

## Notes

* distance min : 2 cm
* distance max : 400 cm
