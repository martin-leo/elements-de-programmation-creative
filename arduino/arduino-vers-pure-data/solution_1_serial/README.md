# Communication avec Pure-Data par le biais de la liaison série

La liaison série permet de communiquer à pure-data des valeurs entre 0 à 255 (2^8).

## Limites

L'étendue de ce qui est communicable est assez faible, mais peut suffire selon le projet.

## Prérequis

* Le logiciel [arduino](https://arduino.cc)
* Le logiciel [pure-data](https://puredata.info)
