/*
   Ce sketch permet d'envoyer à pure-data, via la liaison série, une valeur sur deux octets 2⁸, c'est à dire de 0 à 255.
*/

void setup() {
  /* Fonction appelée une unique fois par Arduino, mais en tout premier */
  Serial.begin(9600);
}

void loop() {
  /* Fonction appelée en boucle par Arduino après la fonction setup */

  // On envoie via la liaison série un nombre de 0 à 255
  Serial.write( random( 256 ) );

  // on attend 1000 millisecondes
  delay(1000);
}
