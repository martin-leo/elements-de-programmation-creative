# Lecture de la variation de luminosité détectée par une photorésistance

## Prérequis

* Le logiciel [arduino](https://arduino.cc)
* Une carte arduino
* Une photorésistance
* Une résistance 10K

## Schéma

![Schéma manquant](arduino-photoresistance_bb.png "Schéma")

## Branchements

```
(a) = arduino

(a) +5V ╾――― photorésistance ――┬―― résistance 10K ―――╼ GND (a)
                               │
                               ┕ pin Analog 0 (A0) (arduino)
```

## Notes

Cette documentation est basée sur [celle d'arduino.cc](http://playground.arduino.cc/Learning/PhotoResistor).
