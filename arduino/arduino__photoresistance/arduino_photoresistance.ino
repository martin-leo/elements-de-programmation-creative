/*
   Ce sketch permet de lire la valeur d'une photorésistance.

   Schéma :

   (arduino) +5V ╾――― photorésistance ――┬―― résistance 10K ―――╼ GND (arduino)
                                        │
                                        ┕ pin Analog 0 (A0) (arduino)

   Attention, on utilise ici bien les pins "analog in"
   au nombre de 6 et marqués A0, A1, A2, etc.
   Ne confondez pas avec les pins "digital" au nombre de 13
*/

// on déclare un entier correspondant au n° de notre pin
int pin_photoresistance = 0; // ici le pin (A)0

void setup() {
  /* Fonction appelée une unique fois par Arduino, mais en tout premier */
  Serial.begin(9600);  // On démarre la liaison série
}

void loop() {
  /* Fonction appelée en boucle par Processing après la fonction setup */
  Serial.println( analogRead(pin_photoresistance) ); // on écrit la valeur dans le moniteur série du pin analog choisi
  delay(10); // un délai que ça tourne pas trop vite
}
