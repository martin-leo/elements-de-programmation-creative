/*
Ce programme écrit une chaîne de caractère donnée, puis l'efface, caractère après caractère.
*/

// on importe la librairie KeyboardAzertyFr, disponible dans Croquis → Inclure une bibliothèque → Gérer les bibliothèques
// Il s'agit de l'adaptation de la librairie Keyboard pour clavier azerty français
#include <KeyboardAzertyFr.h>

// une chaîne de caractère
String chaine_de_caracteres = "bonjour";

// la taille de la chaîne de caractère
int taille_chaine_de_caracteres = chaine_de_caracteres.length();

// la fonction suivante est exécutée une fois, au lancement du programme
void setup () {
  // on démarre l'émulation d'un clavier
  KeyboardAzertyFr.begin();

  // on attend une seconde
  delay(1000);

  // impression d'une chaîne de caractères voir
  // arduino.cc/en/Reference/KeyboardPrint
  KeyboardAzertyFr.print( chaine_de_caracteres );

  // on attend une seconde
  delay(1000);

  // on appuie sur la touche effacer autant de fois qu'on a écrit de caractères précédemment
  for (size_t i = 0; i < taille_chaine_de_caracteres; i++) {
    // envoi d'un caractère spécial, voir
    // arduino.cc/en/Reference/KeyboardModifiers
    KeyboardAzertyFr.write(178); // KEY_BACKSPACE
    delay(250);
  }
}

// la fonction suivant est exécutée en boucle, une fois setup() terminée
void loop () {
}
