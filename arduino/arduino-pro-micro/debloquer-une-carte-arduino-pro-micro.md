# Débloquer une carte Arduino Pro Micro

La carte Arduino Pro Micro existe en deux versions : 5v/16mHz et 3.3v/8mHz. Si vous envoyez un programme compilé pour la mauvaise carte, vous ne pourrez plus télécharger de programme directement.

Pour corriger cela, connectez un interrupteur (pour la praticité) entre le port « reset » et la masse « GND ». Réglez bien la compilation pour la bonne carte, appuyer sur « télécharger » et, juste avant la phase de téléchargement, appuyez deux fois sur l'interrupteur.

Tout est affaire de timing. Si vous appuyez trop tôt, le bootloader risque de quitter avant la fin du téléchargement. Trop tard, la carte ne sera pas trouvée. Si vous êtes bon, vous devriez revoir les deux del rouge de la carte clignoter à un moment ou un autre. La carte devrait être à nouveau utilisable.
