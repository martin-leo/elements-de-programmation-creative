# De processing vers pure-data

Il s'agit ici de faire communiquer **arduino** avec **pure-data**.

## Solutions possibles

Plusieurs solutions existent, en voici une liste absolument pas exhaustive :

* Par liaison série simple : permet d'envoyer simplement des messages de 0 à 255 (2^8) vers pure-data.
* Via Firmata : il s'agit d'un protocole plus évolué et moins évident permettant de communiquer directement avec la carte, et notamment de récupérer les entrées de la carte.
