function ajouter_zeros ( n, nombre_de_zeros ) {
  /* Ajoute des zéros à l'avant d'un nombre
     exemple : 1 -> 001
     Number, Number -> String */
  var zeros = '';
  var iterations = nombre_de_zeros;
  while ( iterations > 0 ) { zeros += '0'; iterations--; }
  return String( zeros + n ).slice( -1 * nombre_de_zeros );
}
