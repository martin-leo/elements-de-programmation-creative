// on ajoute un écouteur d'événement au document
// celui-ci écoute l'événement "touche du clavier relâchée"
// (premier paramètre de addEventListener)
// et déclenche la fonction donnée en second paramètre

document.addEventListener( 'keyup', function ( evenement ) {
  // on récupère la touche qui a été relâchée
  var touche = evenement.key;

  // et on teste celle-ci
  switch (touche) {

    // si c'était la touche 'a'
    case 'a' :
      // on exécute les  instructions suivantes
      console.log('la touche "a" a été relâchée');
      break; // on signale la fin des instruction pour le cas a

    // si c'était la touche 'b'
    case 'b' :
      // on exécute les  instructions suivantes
      console.log('la touche "b" a été relâchée');
      break; // on signale la fin des instruction pour le cas b
  }
}, false);
