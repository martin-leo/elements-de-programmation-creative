/** Représente un cache : un rectangle SVG avec un trou circulaire */
class Cache {
   /**
     * Créé le cache.
     * @param {number} [rayon=100] - Le rayon du trou circulaire
     * @param {string} [selecteur='.dessin'] - Le sélecteur correspondant au conteneur du svg
     * @param {string} [couleur='black'] - La couleur du cache
     */
   constructor ( rayon = 100, selecteur = '.dessin', couleur = 'black' ) {

      // paramètres
      this.rayon = rayon; // le rayon du cercle
      this.largeur = document.body.querySelector( selecteur ).offsetWidth; // la largeur du cache
      this.hauteur = document.body.querySelector( selecteur ).offsetHeight; // sa hauteur
      this.selecteur = selecteur;

      // mise en place du SVG avec svg.js
      this.dessin = SVG().addTo( selecteur ).size( this.largeur, this.hauteur );
      this.chemin = this.dessin.path(this.chemin_rectangle ( this.largeur, this.hauteur )).fill( couleur )
      this.cible = this.dessin.node.querySelector('path');

      // gestion du survol avec un événement mousemove sur le svg
      let self = this;
      this.dessin.node.addEventListener( 'mousemove', function( evenement ) {
         var coordonnees = evenement.target.getBoundingClientRect();
         self.dessiner( evenement.layerX - coordonnees.x, evenement.layerY - coordonnees.y );
      } );

      // gestion à la sortie (on n'affiche plus le cercle)
      this.dessin.node.addEventListener( 'mouseout', function( evenement ) {
         self.dessiner();
      } );

      window.addEventListener('resize', function(){
         self.largeur = document.body.querySelector( self.selecteur ).offsetWidth; // la largeur du cache
         self.hauteur = document.body.querySelector( self.selecteur ).offsetHeight; // sa hauteur
         self.dessin.node.setAttribute( 'width', self.largeur );
         self.dessin.node.setAttribute( 'height', self.hauteur );
      })
   }

   /**
     * Dessine le cache.
     * @param {string} [x] - La position en x du trou circulaire
     * @param {string} [y] - La position en y du trou circulaire
     * @param {number} [rayon=this.rayon] - Le rayon du trou circulaire
     */
   dessiner ( x, y, rayon = this.rayon) {
      /* dessine le cercle à une coordonnée donnée */

      // le chemin
      let chemin = "";

      // on ajoute le chemin du cache
      chemin+= this.chemin_rectangle();

      // si position donnée
      if ( !(x === undefined || y === undefined) ) {

         // on ajoute le chemin du cercle
         chemin += " " + this.chemin_cercle( x, y, rayon );
      }

      // on modifie le chemin dans le svg
      this.cible.setAttribute('d', chemin );
   }

   /**
     * Détermine le chemin svg du rectangle.
   */
   chemin_rectangle () {
      /* retourne une chaine de caractère correspondant à des instructions SVG pour dessiner un rectangle */
      return "M0,0 L0,"+this.hauteur+" L"+this.largeur+","+this.hauteur+" L"+this.largeur+",0 L0,0";
   }

   /**
     * Détermine le chemin svg du cercle.
     * @param {string} [x] - La position en x du trou circulaire
     * @param {string} [y] - La position en y du trou circulaire
     * @param {number} [rayon=this.rayon] - Le rayon du trou circulaire
     */
   chemin_cercle ( x, y, rayon = this.rayon ) {
      /* retourne une chaine de caractère correspondant à des instructions SVG pour dessiner un cercle */
      return "M "+(x-rayon/2)+", "+(y)+" a "+rayon/2+","+rayon/2+" 0 1,1 "+rayon+",0 a "+rayon/2+","+rayon/2+" 0 1,1 -"+rayon+",0";
   }
}
