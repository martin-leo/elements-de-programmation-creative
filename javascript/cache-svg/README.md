# Cache SVG

Ce programme permet d'afficher un cache en svg avec une ouverture qui suit le curseur.

⚠️ Le script utilise un CDN mais vous pouvez télécharger le fichier [svg.js](svgjs.dev) en local.
