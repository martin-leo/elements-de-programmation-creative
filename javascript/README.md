# JavaScript

Ce dossier comprend différentes choses en JavaScript.

## fonctions utiles

* [ajouter des zéros devant un nombre](ajouter-des-zeros-devant-un nombre.js), par exemple pour passer de ```1``` à ```'01'``` ou encore ```0001```.
* [obtenir une date sous forme de chaîne](obtenir-une-date-sous-forme-de-chaine.js), par exemple l'afficher sur une page web, dans un format compréhensible par un humain.
