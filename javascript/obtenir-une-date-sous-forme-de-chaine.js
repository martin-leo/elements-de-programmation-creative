function date ( date ) {
  /* Renvoie une chaîne de caractère contenant la date donnée formatée JJ/MM/AAAA hh:mm
    objet Date -> String
    voir aussi : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date */


    // le jour (1-31)
    var JJ = ajouter_zeros ( date.getDate(), 2 );

    // le mois (1-12)
    var MM = ajouter_zeros ( date.getMonth() + 1, 2 );

    // l'année
    var AAAA = date.getFullYear();

    // l'heure (0-23)
    var hh = ajouter_zeros ( date.getHours(), 2 );

    // les minutes
    var mm = ajouter_zeros ( date.getMinutes(), 2 );

    /* bien qu'on ne les utilise pas ici, on pourrait aussi extraire secondes et minutes

    // les secondes
    var ss = ajouter_zeros ( date.getSeconds() );

    // les millisecondes
    var ms = date.getMilliseconds();

    */

    // on retourne la date donnée sous la forme d'une chaîne de caractère formatée
    return JJ + '/' + MM + '/' + AAAA + ' ' + hh + ':' + mm;
}

//on créé une date correspondant à l'instant présent
var date_actuelle = new Date();

console.log( date ( date_actuelle ) );

/* La fonction suivante permet d'obtenir 01/01/2017 plutôt que 1/1/2017 */

function ajouter_zeros ( n, nombre_de_zeros ) {
  /* Ajoute des zéros à l'avant d'un nombre
     exemple : 1 -> 001
     Number, Number -> String */
  var zeros = '';
  var iterations = nombre_de_zeros;
  while ( iterations > 0 ) { zeros += '0'; iterations--; }
  return String( zeros + n ).slice( -1 * nombre_de_zeros );
}
