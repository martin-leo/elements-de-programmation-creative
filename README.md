# Éléments de programmation créative

Ce dépôt rassemble de petits programmes commentés correspondant à des besoins récurrents en programmation créative.

## Logiciels et technologies concernées

* arduino
* processing
* pure-data
* kinect
* capteurs et actionneurs divers
* html5
* node.js
* express.js
* socket.io
