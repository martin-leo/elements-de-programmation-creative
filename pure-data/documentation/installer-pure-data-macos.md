# Installer Pure Data et des librairies Pure Data sur MacOS

## Télécharger Pure Data

Pure-data peut être télécharger sur [la page downloads](https://puredata.info/downloads) du site [puredata.info](puredata.info) ou sur [la page prévue à cet effet](http://msp.ucsd.edu/software.html) et mise en ligne par Miller Puckette.

`Autrefois, on pouvait conseiller de télécharger Pd-extended, qui était une distribution combinant pure-data et plusieurs bibliothèques utiles. Pd-Extended n'est plus maintenue depuis 2013.`

## Installer des bibliothèques avec Deken

Pure Data est fourni, depuis la version 0.47, avec [Deken](https://github.com/pure-data/deken), un système de management de paquets minimal pour Pure Data.

### Ouvrir Deken via *Help → Find Externals*

![Menu Help → Find Externals](images/0-help__find-externals.png)

### Taper sa recherche et installer le paquet choisi

![Fenêtre Deken pour la recherche osc](images/1-find-externals.png)

### Ajouter le répertoire téléchargé dans la liste des chemins (*paths*)

> Attention : certaines librairies, comme Gem, doivent être chargées au démarrage, faute de quoi elles ne fonctionneront pas. Pour les charger au démarrage, on renseigne leur chemin dans ```Preferences → Startup```

![Menu Pd → Preferences → Path](images/2-Pd__Path.png)

### Chercher le répertoire */Library/Pd/nom-de-la-librairie*

![fenêtre Add a new path](images/3-add-a-new-path.png)
