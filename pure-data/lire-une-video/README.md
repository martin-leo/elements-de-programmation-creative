# Lire une vidéo avec pure-data

Ce patch montre une simple lecture de vidéo, en boucle, dans pure-data.

## Prérequis

* Le logiciel [pure-data](https://puredata.info)
* La librairie Gem (s'il le faut renseignée dans Préférences → Startup)
