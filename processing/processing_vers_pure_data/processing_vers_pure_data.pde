/*
  Cet exemple démontre la transmission de messages
  entre processing et pure-data
  par le biais du protocole Open Sound Control.

  Basé sur oscP5message par andreas schlegel
  http://www.sojamo.de/libraries/oscp5/examples/oscP5message/oscP5message.pde
*/

/* Import des librairies oscP5 et netP5 */
import oscP5.*;
import netP5.*;

/* Création des objets utiles */
OscP5 oscP5;
NetAddress adresse_reseau_cible;

// chaînes de caractères que l'on va envoyer aléatoirement à pure-data
String[] chaines = { "bonjour", "salut", "coucou" };

/* Paramètres */
int port = 9001; // on pourrait utiliser un autre port,
                 // celui-ci correspond à un port vu dans un exemple,
                 // mais ne correspond pas à un réel choix
String adresse_reseau = "127.0.0.1"; // 127.0.0.1 correspond à l'adresse interne votre l'ordinateur

void setup() {
  /* Fonction appelée une unique fois par Processing, mais en tout premier */

  /* Démarrage d'oscP5, et écoute du port donné */
  oscP5 = new OscP5(this, port);

  /* Création d'un objet netAddress */
  adresse_reseau_cible = new NetAddress(adresse_reseau, port);
}

void draw() {
  /* Fonction appelée en boucle par Processing après la fonction setup */

  // on envoie un message avec
  // avec le préfixe "msg",
  // un entier au hasard entre 0 et 100,
  // un float entre 0 et 1,
  // et une chaîne de caractère au hasard
  // parmi celles définis dans setup
  envoyer_message("msg", int( random(100) ), random(1), chaines[ int( random( chaines.length ) ) ]);

  // avec le préfixe "autre route"
  envoyer_message("autre_préfixe", int( random(100) ), random(1), chaines[ int( random( chaines.length ) ) ]);

  // avec le préfixe "autre route"
  envoyer_message("encore un autre préfixe", int( random(100) ), random(1), chaines[ int( random( chaines.length ) ) ]);

  // on attend 1000 millisecondes avant de répéter l'opération
  delay(1000);
}

void envoyer_message( String prefixe, int nombre_entier, float nombre_a_virgule, String chaine_de_caracteres ) {
  /* Envoie un message OSC
     int, float, String -> void */

  /* On démarre le message avec un préfixe démarrant par / pour unpackOSC sur pure-data */
  OscMessage message_osc = new OscMessage("/" + prefixe);

  /* On ajoute un élément de type int */
  message_osc.add( nombre_entier );

  /* On ajoute un élément de type float */
  message_osc.add( nombre_a_virgule );

  /* un autre élément de type String */
  message_osc.add( chaine_de_caracteres );

  /* envoi du message */
  oscP5.send(message_osc, adresse_reseau_cible);

}