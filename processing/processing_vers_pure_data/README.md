# De processing vers pure-data

Il s'agit ici de faire communiquer **processing** avec **pure-data**

## Prérequis

* Le logiciel [processing](https://processing.org)
* La librairie [oscP5](https://www.sojamo.de/libraries/oscp5/) pour processing
* Le logiciel [pure-data](https://puredata.info)
* les librairies **iemnet** et **osc** pour pure-data

La librairie processing oscP5 doit être placé dans le répertoire *sketchbook/libraries*.

Les librairies pure-data sont présentes, si installées, dans le répertoire */usr/lib/pd/extra/* sur Linux et leur chemin de chaque librairie peut nécessiter d'être renseigné dans **Préférence** → **Paths** → **Ajouter** → */usr/lib/pd/extra/**répertoire de la librairie***.
