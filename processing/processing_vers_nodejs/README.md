# Processing vers nodeJS

Cet exemple démontre la communication entre processing et nodejs via socket.io.

## étapes

### 1. installer les paquets nécéssaires à node avec npm

Pour cela, et avec le terminal, se placer dans le répertoire, et lancer :

```npm install```

### 2. lancer le serveur node

```node serveur.js```

### 3. lancer le croquis Processing

Les informations échangées doivent normalement apparaître dans la console node et celle de Processing.
