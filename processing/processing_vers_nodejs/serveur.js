// on créé un serveur http local simple
var server = require('http').createServer();

// on créé le serveur socket.io et on l'attache à notre serveur http
var io = require('socket.io')(server);

io.sockets.on('connection', function ( socket ) {
  /* Lors d'une connection */

  // on signale la connection dans la console
  console.log('nouveau client connecté');

  // on envoie un message au client nouvellement connecté
  io.to(socket.id).emit('exemple-processing-websocket', 'vous êtes connecté');

  // si le client se déconnecte
  socket.once('disconnect', function () {

    // on signale la déconnection dans la console
    console.log('déconnection client');
  });

  // si l'on reçoit un élément préfixé 'exemple-processing-websocket'
  socket.on('exemple-processing-websocket', function( data ){

    // on affiche les données reçues
    console.log( data.message );
  });

});


// on écoute le port 3000
server.listen(3000);
