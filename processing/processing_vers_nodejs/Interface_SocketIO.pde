/*
  Petit module pour s'interfacer avec socket.io
  2017-05-16, Léo MARTIN

  basé sur  https://github.com/alexandrainst/processing_websockets/issues/3
*/
class Interface_SocketIO {
  /* Classe d'interfacage avec socket.io */

  WebsocketClient client_websocket;

  Interface_SocketIO ( WebsocketClient client_websocket ) {
    /* Constructeur, paramètre l'interface
    WebsocketClient  -> void */
    this.client_websocket = client_websocket;
  }

  void envoyer_message( String prefixe, String message ) {
    /* envoie un message avec le préfixe donné
    String -> void */
    try {
        this.client_websocket.sendMessage("42[\"" + prefixe + "\",{\"message\":\"" + message + "\"}]");
    } catch (Exception e) {
        println(e);
    }
  }

  JSONObject traitement_message ( String message ) {
    /* */
    // on extrait les deux premiers caractères
    // qui correspondent à un code
    String code_message = message.substring(0,2);

    if ( message.length() >= 3 && code_message.equals("42") ) {

      // le reste correspond à la chaîne json envoyée
      String json = message.substring(2,message.length());

      // on sépare les données en utilisant les double apostrophes droites
      String[] resultat_parsing = json.split("\"");
      // on prend le second élément (le premier réel élément du json)
      String type = resultat_parsing[1];

      String[] resultat_parsing_2 = json.split(",");

      String valeurs = "";

      // on boucle en passant la première valeur
      for(int i = 1; i < resultat_parsing_2.length ; i++) {
        // si la variable pour les valeurs est encore vide
        if( valeurs.equals("") ) {
          // on ajoute juste la valeur
          valeurs = resultat_parsing_2[i];
        }
        // sinon
        else {
          //on fait de même mais on intercalle une virgule
          valeurs = valeurs +","+ resultat_parsing_2[i];
        }
      }

      // on enlève le ] qui reste à la fin
      valeurs = valeurs.substring(0,valeurs.length()-1);
      json = "{\"prefixe\":\""+type+"\",\"contenu\":"+valeurs+"}";

      return parseJSONObject(json);
    } else {
      return parseJSONObject("{}");
    }
  }
}
