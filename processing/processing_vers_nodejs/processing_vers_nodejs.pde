// on importe la librairie websockets (à installer)
import websockets.*;

// on créé une variable appropriée pour notre client
WebsocketClient client_websocket;

// et une pour notre interface
Interface_SocketIO interface_websocket;

// on renseigne notre hôte, ici localhost
String hote = "localhost"; // pourrait aussi être "127.0.0.1", "192.168.1.250", etc...

// on renseigne le port, ici 3000
int port = 3000;

void setup () {
  /* cette fonction est exécutée une unique fois par Processing, en tout premier */

  // on initialise notre clien websocket
  client_websocket = new WebsocketClient(this, "ws://" + hote + ":" + port + "/socket.io/?EIO=3&transport=websocket");

  // ainsi que notre interface
  interface_websocket = new Interface_SocketIO( client_websocket );
}

void draw () {
  /* fonction appelée en boucle par Processing une fois la fonction setup() exécutée une fois */

  // arguments : préfixe, message
  // ici on va envoyer la position de la souris en x
  interface_websocket.envoyer_message( "exemple-processing-websocket", str( mouseX ) );

  // délai d'une seconde pour ne pas saturer d'information
  delay(1000);
}

void webSocketEvent ( String message ) {
  /* fonction appelée par Processing lorsque celui-ci détecte un événement WebSocket (en gros, un message entrant)
  String -> Void */

  // on imprime le message brut dans la console de Processing
  println("message reçu : `" + message + "`");

  // et la version traitée par notre module interface
  println( "message après traitement : `" +interface_websocket.traitement_message ( message ) +"`" );

}
