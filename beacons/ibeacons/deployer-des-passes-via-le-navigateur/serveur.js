// modules nécessaires
var http =  require('http');
var fs = require('fs');
var path = require('path');

// type MIME des passes
const mime_pkpass = { 'content-type': 'application/vnd.apple.pkpass'};
const repertoire_a_servir = 'passes-generes'

// on créé un serveur
http.createServer( function( requete, reponse ) {

  // pour toute requête

  // on affiche un message indiquant la requête
  console.log( `requête ${requete.method} pour ${requete.url}` );

  // si la requête correspond à un fichier .pkpass
  if (requete.url.match(/.pkpass$/)) {

    // on génère le chemin complet du fichier
    var chemin_du_fichier = path.join(__dirname, repertoire_a_servir, requete.url);

    // on créé un canal pour la réponse
    var file_stream = fs.createReadStream(chemin_du_fichier);

    // on écrit l'entête de notre réponse
    // 200 : OK
    // + type MIME d'un .pkpass
    reponse.writeHead(200, mime_pkpass);

    // on envoie
    file_stream.pipe(reponse);

  // dans tous les autres cas
  } else {
    // on écrit l'entête de notre réponse
    // 404 : non trouvé
    // +type MIME pour du texte brut
    reponse.writeHead(404, {'content-Type': 'text/plain'});

    // on ferme la réponse avec le texte suivant
    reponse.end('404 File Not Found');
  }

// on écoute le port 8080
}).listen(8080);
