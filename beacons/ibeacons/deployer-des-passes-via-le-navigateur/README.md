# Déployer des passes via le navigateur

Ce petit programme permet de déployer des passes passKit vers des périphériques iOS via Safari, en précisant le bon type MIME.

Pour l'utiliser :

* dans votre répertoire de travail, créez un fichier ```passes_generes``` où vous placerez vos fichiers ```.pkpass```
* placez le fichier serveur.json dans votre répertoire de travail
* placez-vous avec le terminal dans votre répertoire de travail, et lancez le serveur en faisant ```node serveur.js```
* repérez votre adresse sur le réseau local et accéder à vos fichiers.pkpass à l'adresse ```mon-adresse-ip-locale:8080/nom-du-fichier.pkpass```.
